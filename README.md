# Subtitles for the *Mara Monarca* YouTube channel

This git repository contains all the subtitle files that I created for **[Mara's YouTube channel](https://www.youtube.com/channel/UC2VLPJ-hcRFFRYTAOQnnAiA)**.

* There are folders for each video.
* Usually there will be two files in each folder, for the two languages I make subtitles for: English and Portuguese!
* All files are in the **[WebVTT](https://en.wikipedia.org/wiki/WebVTT)** format.

```vtt
00:00.260 --> 00:01.820
Hi guys!

00:01.820 --> 00:02.880
My name is Mara
```
(As seen in the *Princess Photoshoot* video.)
